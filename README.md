![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

## Bookmarklet: Easy Follow across Mastodon Instances

This page makes it easier to levarage Bramus's bookmarklet, especially for non-tech minded people.

Let's make Mastodon inclusive and easy-to-use for all!

## Acknowledgements

This code is a very basic attempt to make it easier to generate a Bookmarklet pulling the code from the original gist:

https://gist.github.com/bramus/d8bce55dab1881cde18aa2169c66ac33

by [@bramus@front-end.social](https://front-end.social/@bramus).
